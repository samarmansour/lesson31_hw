﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson31_hw
{
    class SchoolDAO:ISchoolDAO
    {
        private string m_connString;

        public SchoolDAO(string connString)
        {
            m_connString = connString;
        }

        private List<Student> ExecuteNonQuery(string query)
        {
            List<Student> stud = new List<Student>();
            using (SqlCommand cmd = new SqlCommand(query))
            {
                using (cmd.Connection = new SqlConnection(m_connString))
                {
                    cmd.Connection.Open();
                    cmd.CommandType = System.Data.CommandType.Text;
                    SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.Default);
                    
                    while (reader.Read()==true)
                    {
                        stud.Add (
                            new Student{
                                StudentID = Convert.ToInt32(reader["STUDENT_ID"]),
                                Class_ID = Convert.ToInt32(reader["CLASS_ID"]), 
                                Name = reader["NAME"].ToString(),
                                Age = Convert.ToInt32(reader["AGE"]),
                                AddressCity = reader["ADDRESS_CITY"].ToString(),
                                VIP = (bool)reader["VIP"]
                        });
                    }
                }
            }
            return stud;
        }

        private Classroom ExecuteNonQueryClassroom(string query)
        {
            Classroom classroom = new Classroom();
            using (SqlCommand cmd = new SqlCommand(query))
            {
                using (cmd.Connection = new SqlConnection(m_connString))
                {
                    cmd.Connection.Open();
                    cmd.CommandType = System.Data.CommandType.Text;
                    SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.Default);

                    while (reader.Read() == true)
                    {
                        var c = new Classroom
                        {
                            ID = Convert.ToInt32(reader["STUDENT_ID"]),
                            Code = Convert.ToInt32(reader["CODE"]),
                            Name = reader["NAME"].ToString(),
                            NumberOfStudents = Convert.ToInt32(reader["NUMBER_OF_STUDENTS"]),
                            NumberOf_VIP = Convert.ToInt32(reader["NUMBER_OF_VIP"]),
                            AgeAverage = Convert.ToInt32(reader["AGE_AVERAGE"]),
                            MostPopularCity = reader["MOST_POPULAR_CITY"].ToString(),
                            Old_VIP = (int)reader["OLDEST_VIP"],
                            Young_VIP = (int)reader["YOUNGEST_VIP"]
                        };
                        classroom = c;
                    }
                }
            }
            return classroom;
        }

        public Dictionary<Classroom,List<Student>> GetMapClassToStudntsDictionary()
        {
            Dictionary<Classroom, List<Student>> result = new Dictionary<Classroom, List<Student>>();

            result.Add(ExecuteNonQueryClassroom("SELECT NAME FROM Class s JOIN Class c ON c.ID = s.CLASS_ID"),
                       ExecuteNonQuery("SELECT * FROM Students s JOIN Class c ON c.ID = s.CLASS_ID"));

            return result;
        }

        public List<Student> GetStudentsFromClass(int id)
        {
            return ExecuteNonQuery($"SELECT * FROM Students s JOIN Class c ON c.ID = s.CLASS_ID WHERE c.ID = {id}");
        }
    }
}
