﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson31_hw
{
    class Classroom
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Code { get; set; }
        public int NumberOfStudents { get; set; }
        public int NumberOf_VIP { get; set; }
        public int AgeAverage { get; set; }
        public string MostPopularCity { get; set; }
        public int Old_VIP { get; set; }
        public int Young_VIP { get; set; }

        public Classroom()
        {
        }

        public override string ToString()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }
    }
}
