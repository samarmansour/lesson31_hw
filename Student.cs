﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson31_hw
{
    class Student
    {
        public int StudentID { get; set; }
        public int Class_ID { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string AddressCity { get; set; }
        public bool VIP { get; set; }

        public Student()
        {
        }

        public override string ToString()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }
    }
}
